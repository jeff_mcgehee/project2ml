__author__ = 'Jeff'

import random

from deap import base
from deap import creator
from deap import tools

import multiprocessing

import time

import scipy.optimize.anneal

from scipy.optimize import fmin_powell

import numpy as np

class NetOptimizer(object):
    def __init__(self, net, inputs, targets, fitness=None):
        self.net = net
        self.inputs = inputs
        self.targets = targets

        if fitness:
            self.fitness = fitness
        else:
            self.fitness = self._squared_error

        self.final_weights = None



    def _squared_error(self, weights):

        self.net._setParameters(np.asarray(weights))

        outputs=[]
        for val in self.inputs:
            outputs.append(self.net.activate(val))

        # print(outputs)

        error = np.sum((self.targets-outputs)**2)

        return error

    def make_predictions(self):
        outputs = []
        for val in self.inputs:
            temp_out = self.net.activate(val)
            single_output = []
            for out_val in temp_out:

                # single_output.append(bias_val[0]*out_val)
                if out_val > 0.5:
                    single_output.append(1.)
                else:
                    single_output.append(0.)
            outputs.append(single_output)

        return outputs




class GeneticAlgorithmOptimizer(NetOptimizer):
    def __init__(self, net, inputs, targets, fitness=None):
        super(GeneticAlgorithmOptimizer, self).__init__(net, inputs, targets, fitness)

        self.toolbox = base.Toolbox()

        if self.fitness == self._squared_error:
            print("Fixing Error")

            self.fitness = self._squared_error_fix

    def _squared_error_fix(self, weights):
        return self._squared_error(weights),

    def _initial_weights(self):
        if random.random > 0.5:
            return random.random()*100
        return random.random()*-100


    def train(self, num_pop=50, generations=40, crossover_prob=0.5, mutation_prob=0.5):

        creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)


        # Attribute generator
        self.toolbox.register("attr_float", self._initial_weights)
        # Structure initializers
        self.toolbox.register("individual", tools.initRepeat, creator.Individual,
            self.toolbox.attr_float, self.net.params.shape[0])
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)



        # Operator registering
        self.toolbox.register("evaluate", self.fitness)
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutGaussian, mu=5., sigma=5., indpb=0.05)
        # self.toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
        self.toolbox.register("select", tools.selTournament, tournsize=3)




        pop = self.toolbox.population(n=num_pop)


        print("Start of evolution")



        # Evaluate the entire population
        fitnesses = list(map(self.toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        print("  Evaluated %i individuals" % len(pop))

        # Begin the evolution
        for g in range(generations):
            print("-- Generation %i --" % g)
            start = time.time()

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))

            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < crossover_prob:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values

            for mutant in offspring:
                if random.random() < mutation_prob:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            print("  Evaluated %i individuals" % len(invalid_ind))

            # The population is entirely replaced by the offspring
            pop[:] = offspring

            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]

            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x*x for x in fits)
            std = abs(sum2 / length - mean**2)**0.5

            print("  Min %s" % min(fits))
            print("  Max %s" % max(fits))
            print("  Avg %s" % mean)
            print("  Std %s" % std)

            print("  Time: %0.3f"%(time.time()-start))

        print("-- End of (successful) evolution --")

        best_ind = tools.selBest(pop, 1)[0]
        print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))

        self.final_weights = best_ind


        return best_ind





class SimulateAnnealingOptimizer(NetOptimizer):
    def __init__(self, net, inputs, targets, fitness=None):
        super(SimulateAnnealingOptimizer, self).__init__(net, inputs, targets, fitness)


    def train(self):
        output = scipy.optimize.anneal(self.fitness, self.net.params*100)

        print(self.fitness(output[0]))

        self.final_weights = output[0]

        print(output)


        return self.final_weights




class RandomHillClimberOptimizer(NetOptimizer):
    def __init__(self, net, inputs, targets, fitness=None):
        super(RandomHillClimberOptimizer, self).__init__(net, inputs, targets, fitness)


    def train(self):
        output = fmin_powell(self.fitness, self.net.params*100)

        self.final_weights = output

        print(self.fitness(output))
        print(output)



        return self.final_weights

if __name__ == "__main__":
    pass