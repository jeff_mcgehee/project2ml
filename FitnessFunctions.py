__author__ = 'Jeff'


import matplotlib.pyplot as plt
import numpy as np

import random

import plotly.plotly as py
from plotly.graph_objs import *

py.sign_in('jlmcgehee21','8x8a5b2iwh')

# from mimicry import Mimic


class OptimizationSurface(object):
    def __init__(self, func, x_domain, y_domain, x_interval=1, y_interval=1, opt_points=[]):
        self.func = func

        self.x_domain = x_domain
        self.y_domain = y_domain

        self.x = np.arange(x_domain[0], x_domain[1], x_interval)
        self.y = np.arange(y_domain[0], y_domain[1], y_interval)

        self.z = self.evaluate((self.x, self.y))

        self.opt_points = opt_points

    def evaluate(self, inputs):
        return self.func(inputs)

    @property
    def global_max(self):
        max_inds = np.unravel_index(self.z.argmax(), self.z.shape)
        return np.max(self.z), (self.x[max_inds[0]], self.y[max_inds[0]])

    def plotly_surface(self):
        title = str(self.func.__name__)

        data = Data([
            Surface(
                x=self.x, y=self.y, z=self.z, colorscale='Jet'
            )
        ])
        layout = Layout(
            title=title,
            autosize=False,
            width=500,
            height=500,
            margin=Margin(
                l=65,
                r=50,
                b=65,
                t=90
            )
        )

        fig = Figure(data=data, layout=layout)

        return py.plot(fig, filename=title)




def four_peaks(inputs): # 4 hills in a flat space
    if type(inputs[0]) is np.ndarray:
        X, Y = inputs

    else:
        # try:
        X, Y = inputs[:len(inputs)/2], inputs[len(inputs)/2:]

        X = bin_list_to_int(X)
        Y = bin_list_to_int(Y)

        # except ValueError:
        #
        #     return 0


    X, Y = np.meshgrid(X, Y)
    Z = np.exp(-(((X-100)*0.06)**2)-(((Y-110)*0.06)**2)) + 0.5*np.exp(-(((X-100)*0.06-2)**2)-(((Y-110)*0.06)**2))
    Z += 0.5*(np.exp(-(((X-100)*0.01)**2)-(((Y-190)*0.04)**2)))# + 0.5*np.exp(-(((X+5)-2)**2)-((Y-5)**2))
    Z += 0.7*(np.exp(-(((X-220)*0.05)**2)-(((Y-75)*0.05)**2)))

    return Z


def bin_list_to_int(bin_list):
    str_list = [str(int(item)) for item in bin_list]

    string_item = ''.join(str_list)

    return int(string_item, 2)



class OptimizationImage(object):
    def __init__(self, img_path):
        self.orig_array = plt.imread(img_path)

        self.raw_array = self.orig_array[:,:,3]

        bw_mask = self.raw_array > 0.9

        self.bw_array = 1*bw_mask.astype(int)

        self.target_array = self.bw_array.flatten()

        self.domain = [(0,1)]*self.target_array.shape[0]

    def evaluate(self, test_array):
        return -np.sum((self.target_array-test_array)**2)

    def convert_for_plotting(self, test_array):
        test_array = np.asarray(test_array)
        print(test_array.shape)
        return test_array.reshape(self.orig_array.shape[0], self.orig_array.shape[1])




class Knapsack(object):
    def __init__(self, weight_capacity, packable_items=30, item_weights=None, item_values=None):
        self.weight_capacity = weight_capacity
        self.packable_items = packable_items

        if not item_weights:
            self.item_weights=[]
            while len(self.item_weights) < self.packable_items:
                self.item_weights.append(random.random())
        self.item_weights = np.asarray(self.item_weights)


        if not item_values:
            self.item_values=[]
            while len(self.item_values) < self.packable_items:
                self.item_values.append(random.random())
        self.item_values = np.asarray(self.item_values)

        self.domain = [(0, 1)]*self.packable_items


    def evaluate(self, test_array):
        test_array = np.asarray(test_array, dtype=bool)

        carried_weight = np.sum(self.item_weights[test_array])
        carried_value = np.sum(self.item_values[test_array])

        if carried_weight < self.weight_capacity:
            return carried_value
        else:
            return carried_value - 2*(carried_weight-self.weight_capacity)




if __name__ == '__main__':
    import matplotlib.animation as animation
    import DiscreteOptimizers

    logo = OptimizationImage('img/twitter_logo.png')


    # plt.imshow(logo.raw_array)

    # plt.imshow(logo.bw_array, cmap='binary')

    # print(logo.target_array.shape[0])

    # salesman = TravelingSalesman(5)
    # salesman.evaluate([0,0,0,0,1 , 0,0,0,1,0 , 0,0,1,0,0 , 0,1,0,0,0 , 1,0,0,0,0])

    knapsack = Knapsack(2)
    print(knapsack.evaluate([1, 1]))

    print(np.sum(knapsack.item_weights))

    ga = DiscreteOptimizers.GeneticAlgorithm([(0,1)]*knapsack.packable_items, knapsack.evaluate, pop_size=100)
    # ga = optimizers.GeneticAlgorithm([(0,1)]*salesman.num_nodes**2,salesman.evaluate)

    # ga = optimizers.GeneticAlgorithm([(0, 1)]*logo.target_array.shape[0], logo.evaluate)

    ga.iterate(1000)

    max_inds_ga = np.asarray([results[0] for results in ga.records])
    results_ga = np.asarray([results[1] for results in ga.records])

    print(max_inds_ga[-1])

    # print(max_inds_ga[-1])
    #
    # fig2 = plt.figure()
    #
    #
    # ims = [(plt.imshow(img.reshape(64, 64), cmap='binary'),) for img in max_inds_ga]
    #
    #
    #
    # im_ani = animation.ArtistAnimation(fig2, ims, interval=5, repeat_delay=3000,
    # blit=False)
    #
    # im_ani.save('GA.mp4', metadata={'artist':'Jeff'})

    # import optimize
    #
    # test_func = OptimizationSurface(four_peaks, (0, 50), (0, 50), x_interval=0.2, y_interval=0.2)
    #
    # # test_func.plotly_surface()
    #
    # anneal = optimize.SimulatedAnnealing([test_func.x_domain, test_func.y_domain], test_func.func, temperature=10000000)
    # anneal.iterate(2000)
    #
    # results_anneal = anneal.records
    #
    # print(len(results_anneal))
    #
    # max_inds_anneal = [results[0] for results in results_anneal]
    # results_anneal = [results[1] for results in results_anneal]
    #
    # hill = optimize.RandomHillClimb([test_func.x_domain, test_func.y_domain], test_func.func)
    # hill.iterate(2000)
    #
    # results_hill = hill.records
    #
    # max_inds_rand = [results[0] for results in results_hill]
    # results_rand = [results[1] for results in results_hill]
    #
    #
    # max_inds_gen, results_gen = optimize.geneticoptimize([test_func.x_domain, test_func.y_domain], test_func.func)
    #
    #
    # mimic = Mimic([test_func.x_domain, test_func.y_domain], test_func.func, samples=50, percentile=.75)
    #
    # results_mimic = []
    # for i in range(50):
    #     print(i)
    #     results_mimic.append(mimic.fit()[0])
    #
    # # print(results_mimic)
    #
    # print(np.squeeze(np.asarray(results_mimic)))
    #
    # # max_inds_mimic = [results[0] for results in results_mimic]
    # results_mimic = [test_func.func(results) for results in results_mimic]
    #
    # # print(results_mimic)
    # #
    # # print(max_inds_anneal)
    # # print(max_inds_rand)
    # # print(max_inds_gen)
    #
    # results_anneal = np.asarray(results_anneal).flatten()
    # results_mimic = np.asarray(results_mimic).flatten()
    # # print(results_anneal.shape)
    # results_rand = np.asarray(results_rand).flatten().tolist()
    # results_gen = np.asarray(results_gen).flatten().tolist()
    #
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # ax.plot(results_anneal, label='Annealing')
    # ax.plot(results_rand, label='Random')
    # ax.plot(results_mimic, label='Mimic')
    ax.plot(results_ga, label='Genetic')
    #
    # ax.legend(loc='best')
    #
    plt.show()