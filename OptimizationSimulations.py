__author__ = 'Jeff'

import FitnessFunctions
import DiscreteOptimizers
import mimicry.mimic as mimic

import matplotlib.pyplot as plt

import cPickle as pickle
import os



def knapsack_sim():

    for i in range(10):

        knapsack = FitnessFunctions.Knapsack(10)


        ga = DiscreteOptimizers.GeneticAlgorithm(knapsack.domain, knapsack.evaluate, pop_size=50)

        sa = DiscreteOptimizers.SimulatedAnnealing(knapsack.domain, knapsack.evaluate, temperature=1000., cool=.97)

        rhc = DiscreteOptimizers.RandomHillClimb(knapsack.domain, knapsack.evaluate)

        mim = mimic.Mimic(knapsack.domain, knapsack.evaluate, samples=50, percentile=0.3)

        run_funcs = [ga.iterate, sa.iterate, rhc.iterate, mim.fit]


        ga.iterate(100)
        print('GA Complete')

        sa.iterate(100)
        print('SA Complete')

        rhc.iterate(100)
        print('RHC Complete')

        mim.fit(100)
        print('MIMIC Complete')



        ga_results = ga.records
        sa_results = sa.records
        rhc_results = rhc.records
        mim_results = mim.records

        save_results((ga_results, sa_results, rhc_results, mim_results), 'Knapsack_', str(i))



    ga_inds = [results[0] for results in ga_results]
    sa_inds = [results[0] for results in sa_results]
    rhc_inds = [results[0] for results in rhc_results]
    mim_inds = [results[0] for results in mim_results]

    ga_error = [results[1] for results in ga_results]
    sa_error = [results[1] for results in sa_results]
    rhc_error = [results[1] for results in rhc_results]
    mim_error = [results[1] for results in mim_results]

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.plot(ga_error, label="GA")
    ax.plot(sa_error, label="SA")
    ax.plot(rhc_error, label="RHC")
    ax.plot(mim_error, label="MIM")

    ax.legend(loc="best")

    plt.show()


def img_sim():

    img = FitnessFunctions.OptimizationImage('/Users/Jeff/Desktop/C-responsive_files-social_icons-facebook_icon_white_148579_1.png')


    for i in range(10):
        ga = DiscreteOptimizers.GeneticAlgorithm(img.domain, img.evaluate)

        sa = DiscreteOptimizers.SimulatedAnnealing(img.domain, img.evaluate)

        rhc = DiscreteOptimizers.RandomHillClimb(img.domain, img.evaluate)

        mim = mimic.Mimic(img.domain, img.evaluate, samples=100, percentile=0.3)

        ga.iterate(2000)
        print('GA Complete')

        sa.iterate(2000)
        print('SA Complete')

        rhc.iterate(2000)
        print('RHC Complete')

        mim.fit(200)
        print('MIMIC omplete')

        ga_results = ga.records
        sa_results = sa.records
        rhc_results = rhc.records
        mim_results = mim.records

        save_results((ga_results, sa_results, rhc_results, mim_results), 'Image_', str(i))

    ga_inds = [results[0] for results in ga_results]
    sa_inds = [results[0] for results in sa_results]
    rhc_inds = [results[0] for results in rhc_results]
    mim_inds = [results[0] for results in mim_results]

    ga_error = [results[1] for results in ga_results]
    sa_error = [results[1] for results in sa_results]
    rhc_error = [results[1] for results in rhc_results]
    mim_error = [results[1] for results in mim_results]



    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(img.convert_for_plotting(ga_inds[-1]), cmap='binary')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(img.convert_for_plotting(sa_inds[-1]), cmap='binary')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(img.convert_for_plotting(rhc_inds[-1]), cmap='binary')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(img.convert_for_plotting(mim_inds[-1]), cmap='binary')

    # ax.plot(ga_error, label="GA")
    # ax.plot(sa_error, label="SA")
    # ax.plot(rhc_error, label="RHC")
    # ax.plot(mim_error, label="MIMIC")

    # ax.legend(loc="best")

    plt.show()

def surface_sim():

    surf = FitnessFunctions.OptimizationSurface(FitnessFunctions.four_peaks,(0, 256), (0, 256))

    surf_domain = [(0, 1)]*16
    # surf.plotly_surface()

    for i in range(10):

        ga = DiscreteOptimizers.GeneticAlgorithm(surf_domain, surf.evaluate)

        sa = DiscreteOptimizers.SimulatedAnnealing(surf_domain, surf.evaluate)

        rhc = DiscreteOptimizers.RandomHillClimb(surf_domain, surf.evaluate)

        mim = mimic.Mimic(surf_domain, surf.evaluate, samples=75, percentile=0.3)

        ga.iterate(100)
        print('GA Complete')

        sa.iterate(100)
        print('SA Complete')

        rhc.iterate(100)
        print('RHC Complete')

        mim.fit(100)
        print('MIMIC Complete')

        ga_results = ga.records
        sa_results = sa.records
        rhc_results = rhc.records
        mim_results = mim.records

        save_results((ga_results, sa_results, rhc_results, mim_results), 'Four_Peaks_', str(i))

    ga_inds = [results[0] for results in ga_results]
    sa_inds = [results[0] for results in sa_results]
    rhc_inds = [results[0] for results in rhc_results]
    mim_inds = [results[0] for results in mim_results]

    ga_error = [results[1][0] for results in ga_results]
    sa_error = [results[1][0] for results in sa_results]
    rhc_error = [results[1][0] for results in rhc_results]
    mim_error = [results[1][0] for results in mim_results]

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.plot(ga_error, label="GA")
    ax.plot(sa_error, label="SA")
    ax.plot(rhc_error, label="RHC")
    ax.plot(mim_error, label="MIMIC")

    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)

    ax.legend(loc="best")

    plt.show()


def save_results(results_tuple, description, suffix=''):
    save_dict = dict(ga=results_tuple[0], sa=results_tuple[1], rhc=results_tuple[2], mim=results_tuple[3])

    write_file = open(os.path.join(os.curdir+'/results',description+suffix+'.pck'), 'wb')

    pickle.dump(save_dict, write_file)

    write_file.close()

    return



if __name__ == "__main__":

    # knapsack_sim()
    img_sim()
    # surface_sim()



