__author__ = 'Jeff'

import FormatData
import NetOptimizer

from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import SoftmaxLayer

import numpy as np
import matplotlib.pyplot as plt

import cPickle as pickle

#For all optimization algos, use structure deemed best by project 1

class NetSimulation(object):
    def __init__(self, export_name, train_data, test_data, hidden_nodes=30):

        self.train_data = train_data#FormatData.loadAd()
        self.test_data = test_data
        self.export_name = export_name

        print(self.train_data.outdim)

        self.net = buildNetwork(self.train_data.indim, hidden_nodes, self.train_data.outdim)

    def ga_train(self):

        trainer = NetOptimizer.GeneticAlgorithmOptimizer(self.net,
                                                         self.train_data.inputs,
                                                         self.train_data.targets,)


        self.final_weights = trainer.train(generations=10, crossover_prob=0.8)

        self.net._setParameters(self.final_weights)


    def sa_train(self):

        trainer = NetOptimizer.SimulateAnnealingOptimizer(self.net,
                                                          self.train_data.inputs,
                                                          self.train_data.targets,)

        self.final_weights = trainer.train()
        self.net._setParameters(self.final_weights)


    def rhc_train(self):

        trainer = NetOptimizer.RandomHillClimberOptimizer(self.net,
                                                          self.train_data.inputs,
                                                          self.train_data.targets,)

        self.final_weights = trainer.train()
        self.net._setParameters(self.final_weights)

    def test_net(self):

        # outputs=[]
        # for val in self.train_data.inputs:
        #     # print(val)
        #     outputs.append(self.net.activate(val))

        correct = 0
        incorrect = 0
        outputs=[]
        for inval, targval in zip(self.train_data.inputs, self.train_data.targets):
            temp_out = self.net.activate(inval)
            print(targval)
            print(temp_out)


            if np.argmax(temp_out) == np.argmax(targval):
                correct += 1
            else:
                incorrect += 1
            # else:
            #     pass

            # outputs.append(single_output)
            outputs.append(temp_out)

        outputs = np.asarray(outputs).flatten()
        print(outputs.shape)


        print('Correct:', correct)
        print('Incorrect:', incorrect)

        fig = plt.figure()
        ax = fig.add_subplot(2, 1, 1)
        ax.plot(self.train_data.targets.flatten())

        ax1 = fig.add_subplot(2, 1, 2)
        ax1.plot(outputs)

        plt.show()

    def export_outputs(self):

        output_dict = dict(weights=self.final_weights,
                           train_data=self.train_data,
                           test_data=self.test_data)

        with open(export_name, 'wb') as file_obj:
            pickle.dump(output_dict, file_obj)

if __name__ == "__main__":

    set_name = "ad"
    percentage = 0.5
    export_name = "stuff"

    if set_name == "ad":
        data = FormatData.loadAd()

    elif set_name == "heart":
        data = FormatData.load_heart()

    else:
        exit(1)

    data = data.one_hot()

    data = data.reduced_inputs(0.05)

    train_data, test_data = data.shufflesplit(test_percent=0.2)

    # print(data.indim)
    # print(data.targets)
    #
    # new_inputs = []
    # new_targets = []
    # for ind, val in enumerate(data.inputs):
    #     if ind % 10 == 0:
    #         new_inputs.append(val)
    #         new_targets.append(data.targets[ind])
    #
    # data.targets = np.asarray(new_targets)
    # data.inputs = np.asarray(new_inputs)

    analyzer = NetSimulation(export_name, train_data, test_data, 5)

    analyzer.ga_train()

    analyzer.test_net()